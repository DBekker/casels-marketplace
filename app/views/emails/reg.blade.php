<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Welcome to the Casel's Marketplace, {{ $name }}. We're glad to have you on board!</h2>
		<strong>Username:</strong> {{ $username }}
		<br>
		<br>
		<strong>Password:</strong> {{ $password }}
		<br>
		<br>
		<strong>Email:</strong> {{ $email }}
	</body>
</html>