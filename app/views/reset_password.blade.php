@include('header')
<style type="text/css">
.login-container {
  width: 100%;
  height: 100%;
  position: relative;
  /* background: url(images/login-bg.jpg); */
  background-size: cover;
  background-position: 50%;
  background-color: #EFEFEF;
}
	nav {
		display: none;
	}
	#user-lable {
		position: absolute;
		margin-bottom: 0px;
		padding-top: 6px;
		left: 32px;
    display:none;
	}
	#pass-lable {
		position: absolute;
		margin-bottom: 0px;
		padding-top: 26px;
		left: 32px;
    display:none;
	}
  #form-wrapper-login {
    width: 400px;
    position: absolute;
    /* left: 50%; */
    left: 0;
    right: 0;
    top: 50%;
    transform: translate(0,-50%);
    padding: 20px;
    background-color: white;
    border-radius: 5px;
    box-shadow:0 0 10px rgba(0, 0, 0, 0.3);
}
	#userinput {} #password {
		margin-top: 20px;
	}
	#login-logo {
		max-width: 100%;
	}
  #log-head {
    font-family: 'Lithos';
    color: #333;
}
</style>

<div class="login-container">
	@if ($message = Session::get('alert')) {{ $message }} @endif
	<div id="form-wrapper-login">
		<div style="padding-bottom:20px;text-align:center;display:table;">
			<a href=".\">
				<img src={{ URL::asset( 'images/logo-new.png') }} alt="Casel's Logo" id="login-logo" style="padding-top:0;" />
        </a>
        <hr>
        <h1 id="log-head">Reset Password</h1>
		</div>

		{{ Form::open(array('url' => 'forgot/' . $id . '/' . $code . '/confirm', 'style' => 'width: 100%;')) }} 
		{{ Form::label('username', 'Username:', array('id' => 'user-label')) }} 

		{{ Form::text('username', '', array('placeholder' => $user, 'disabled' => 'true', 'name' => 'username', 'tabindex' => '1', 'required' => 'false', 'class' => 'form-control', 'id'
		=> 'userinput','autocomplete' => 'off')) }} 

		{{ Form::label('password', 'New Password:', array('id' => 'user-label')) }} 

		{{ Form::text('password', '', array('name' => 'password', 'tabindex' => '2', 'required' => 'true', 'class' => 'form-control', 'id'
		=> '','autocomplete' => 'off')) }} 

		{{ Form::submit('Login', array('tabindex' => '10', 'class' => 'btn btn-primary', 'style' => ' margin-top: 20px; margin-bottom: 5px;width:100%; background-color: #5191d6;')) }} {{ Form::close() }}
		<a href="{{ URL::to('forgot') }}" style="color: white;float:left;font-size: 12px;">Forgot Password?</a>

		<a href="http://aidevserver.co/projects/casels/public/register" style="color: white;float:right;font-size: 12px;">Register -&gt;</a>

	</div>
</div>
